/*
 * Manuja Gunaratne A10468455 mgunarat
 * Dheeraj Navani A10884691 dvnavani
 *
 */

#include "BitOutputStream.hpp"

//Clean out the bits
void BitOutputStream::flush()
{
    //put th buffer to output
    output.put(byteBuffer);
    //Clear everything
    output.flush();
    byteBuffer = 0;
    numBits = 7;
}

void BitOutputStream::writeBit(int i)
{
    //Check if the buffer is full, if then flush
    if (numBits < 0)
        flush();
    //wirte the least significant bit of iinto the buffer
    byteBuffer = byteBuffer | (i << numBits--);
}

void BitOutputStream::finishIfTrailing()
{
   //Flush out the bits
    if( numBits != 7 )
        flush();
}
