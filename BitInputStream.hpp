/*
 * Manuja Gunaratne A10468455 mgunarat
 * Dheeraj Navani A10884691 dvnavani
 */

#ifndef BITINPUTSTREAM_HPP
#define BITINPUTSTREAM_HPP
#include <iostream>

class BitInputStream
{
private:
    //Create a byteBuffer and the bit number
    char byteBuffer;
    int numBits;
    std::istream &input;

public:
    BitInputStream(std::istream &inputStream) : input(inputStream)
    {
	//Constructure
	//Reset bytes and add to bits
        byteBuffer = 0;
        numBits = -1;
    }

    //Fill the file
    void fill();
    //Read the file
    int readBit();
};

#endif
