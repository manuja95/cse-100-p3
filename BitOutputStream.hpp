/*
 * Manuja Gunaratne mgunarat A10468455
 * Dheeraj Navani dvnavani A10884691
 */
#ifndef BITOUTPUTSTREAM_HPP
#define BITOUTPUTSTREAM_HPP
#include <iostream>

class BitOutputStream
{
private:
    char byteBuffer;
    int numBits;
     
public:
    std::ostream &output;
    //Constructor
    BitOutputStream (std::ostream &outputStream): output(outputStream)
    {
        byteBuffer=0;
        numBits = 7;
    }

    //Clear out the output stresm
    void flush();
    //Write the bit in the buffer
    void writeBit (int i);
    //Finish function
    void finishIfTrailing();
};

#endif
