/*
 * Manuja Gunaratne mgunarat A10468455
 * Dheeraj Navani dvnavani  A10884691
 */


#include "BitInputStream.hpp"

void BitInputStream::fill()
{
    //Get the byte from the input
    byteBuffer = input.get();
    //Set the bits to 7
    numBits = 7;
}

int BitInputStream::readBit()
{
    //Check if there is no bits
    if (numBits < 0)
        fill();
    //Check if end of file if not the byte bugger
    return input.eof() ? -1 : 1&(byteBuffer>>numBits--); 
}
