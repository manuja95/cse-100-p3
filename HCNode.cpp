/*
 * Manuja Gunaratne A10468455 mgunarat
 * Dheeraj Navani A10884691 dvnavani
 *
 */

#include "HCNode.hpp"

//Constructor
bool HCNode::operator < (const HCNode &other)
{
    return count >= other.count; // want reverse of typical <
}
//Compare two nodes
bool comp (HCNode *one, HCNode *other)
{
    return one->count < other->count;
}
