/*
 * Manuja Gunaratne mgunarat A10468455
 * Dheeraj Navani dvnavani  A10884691
 */
#include "HCTree.hpp"
#include <fstream>
#include <vector>

int main( int argc, char * argv[] )
{
    //Check if the correct parameters are being passed in
    if( argc != 3 )
    {
        cerr << "Usage error: " << argv[0] << " infile outfile\n";
        return -1;
    }

    // STEP 1
    // Open the input file for reading
    ifstream infile;
    infile.open(argv[1],ifstream::binary);
    if (infile.is_open()==false)
    { 
	cerr << "Usage Error: Couldn't find the file \n";
    }

    // STEP 2
    // Read the file header at the beginning of the input file, and reconsturct
    // the Huffman Coding Tree
    vector<int> freq(256);
    long filesize = 0;
    bool notEmpty = false;

    if( infile.good() )
    for( int i = 0; i < 256; i++ )
    {
        infile.read((char*) &freq[i],sizeof(int));
        if( freq[i] != 0 )
            notEmpty = true;
        filesize += freq[i];
    }

    HCTree h;
    if( notEmpty )
        h.build(freq);

    // STEP 3
    // Open the output file for writing
    ofstream outfile;
    outfile.open(argv[2],ios::binary);

    // STEP 4
    // Using the Huffman coding tree, decode the bits from the input file into
    // the approrpiate sequence of bytes
    // Write it down to the output file
    BitInputStream inStream(infile);
    int outChar;
    long l = 0;
    if( notEmpty )
    while( ! infile.eof() && l++ < filesize )
    {
        outChar = h.decode(inStream);
        if( outChar == -1 )
            break;
        outfile.put( (byte) outChar );
    }

    // STEP 5
    // Close the input and output files
    infile.close();
    outfile.close();

    return 0;
}
