/*
 * Manuja Gunaratne mgunarat A10468455
 * Dheeraj Navani dvnavani  A10884691
 */

#include "HCTree.hpp"
#include <fstream>

int main (int argc, char* argv[])
{
    // STEP 1 - Open the input file for reading
    if (argc != 3)
    {
	printf("./compress was called with incorrect number of arguments \n");
	return -1; 
    }
    ifstream infile;
    infile.open(argv[1],ifstream::binary);
    //Check if the file is present
    if(infile.is_open() == false)
    {
	printf("Couldn't find the input file \n");
	return -1;
    }   
    // STEP 2 - Read bytes from the file, 
    // counting the number of occurrences of each byte value;
    // then close the file.
    vector<int> frequency(256,0);
    char currentChar;
    bool notEmpty = false;
    long filesize = 0;
    
    while( (! infile.eof()) && infile.get(currentChar).good() )
    {
        frequency[(byte)currentChar]++;
        filesize++;
        notEmpty = true;
    }
    infile.close();
    
    // STEP 3 - Use these byte counts to construct a Huffman coding tree.
    HCTree h;
    if( notEmpty )
        h.build(frequency);
    
    // STEP 4 - Open the output file for writing.
    ofstream outfile;
    outfile.open(argv[2],ofstream::binary);
    
    // STEP 5 - Write enough information (a "file header") to the output file
    // to enable the coding tree to be reconstructed when the file is read
    // by your uncompress program.
    outfile.write((char *) &frequency[0],256*sizeof(int));
    
    // STEP 6 - Open the input file for reading, again.
    infile.open(argv[1],ifstream::binary);
    
    // STEP 7 - Using the Huffman coding tree, translate each byte from the 
    // input file into its code, and append these codes as a sequence of 
    // bits to the output file, after the header.
    BitOutputStream outStream( outfile );
    if( notEmpty )
    while( (! infile.eof()) &&  infile.good() )
    {
        infile.get(currentChar);
        h.encode(currentChar, outStream);
    }
    outStream.finishIfTrailing();

    // STEP 8 - Close the input and output files.
    infile.close();
    outfile.close();

    return 0;
}
