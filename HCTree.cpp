/*
 * Manuja Gunaratne mgunarat A10468455
 * Dheeraj Navani dvnavani A10884691
 *
 */

#include "HCTree.hpp"
void deleteNode(HCNode* n)
{
    //Recursively delete both sides
    if( n != 0 )
    {
        deleteNode(n->c0);
        deleteNode(n->c1);
        delete n;
    }
}
HCTree::~HCTree()
{
    deleteNode(root);
}
void HCTree::build(const vector<int>& freqs)
{
    // STEP 1 - Create a Forest
    priority_queue<HCNode*,vector<HCNode*>,HCNodePtrComp> forest;
    vector<HCNode*>::size_type i;
    //Iterate through the frequency list
    for( i = 0; i < freqs.size(); i++ )
    {
        if( freqs[i] != 0 )
        {
            leaves[i] = new HCNode(freqs[i],i,0,0,0);
            forest.push(leaves[i]);
        }
    }

    if( forest.size() == 1 )
        forest.push(new HCNode(0,0,0,0,0)); 

    // STEP 2 - Loop while there is more than 1 tree in the forest
    while( forest.size() > 1 )
    {
        // STEP 2a - remove the two smallest
        HCNode* smallest = forest.top();
        forest.pop();
        HCNode* second = forest.top();
        forest.pop();
        
        // STEP 2b - create parent and relationships
        HCNode *parent = new HCNode( smallest->count + second->count, 0,
                                     smallest, second, 0 );
        smallest->p = parent;
        second->p = parent;

        // STEP 2c - insert the new node into forest
        forest.push(parent);
    }

    // STEP 3 - return tree
    root = forest.top();
    forest.pop();
    return;
}

//Encode the symbol from the BitInputStream 
void HCTree::encode(byte symbol, BitOutputStream& out) const
{
    // use symbol to retrieve corresponding HCNode
    HCNode* n = leaves[symbol];
    if(!n) return;

    // keep pre-pending additional codes to get the full string
    stack<int> code;
    while( n->p != 0 )
    {
        if( n->p->c0 == n )
            code.push(0);
        else
            code.push(1);

        n = n->p;
    }

    // write the code to output stream bit by bit
    while( code.size() > 0 )
    {
        out.writeBit( code.top() );
        code.pop();
    }

    return;
}

//Decode the bitinputstream
int HCTree::decode(BitInputStream& in) const
{
    // start from root and go down the tree 
    // to find the node signified by the input
    HCNode* currNode = root;
    while( currNode->c0 != 0 || currNode->c1 != 0 )
    {
        //Check which side it goes to
        switch( in.readBit() )
        {
            case 0:
                currNode = currNode->c0;
                break;
            case 1:
                currNode = currNode->c1;
                break;
            default:
                return -1;
        }
    }
    return currNode->symbol;
}
